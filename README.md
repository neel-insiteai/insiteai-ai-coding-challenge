# insiteai-ai-coding-challenge
___

## Problem Statement:
---

Consider below, a 2-dimensional grid of 10 x 10 cells consisting of a single English alphabet per cell. Some combinations of the alphabets in the grid (horizontally, vertically and diagonally) makes up English words (like CAT, DOG, MONKEY).

| **C** | **A** | **T** | K | **M** | X | P | Q | N | G |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| G | N | S | I | U | **O** | S | P | M | X |
| V | I | T | Q | R | D | **N** | S | F | J |
| W | **D** | F | T | U | B | A | **K** | R | S |
| K | **O** | Q | E | R | V | N | P | **E** | S |
| H | **G** | W | G | G | G | G | Y | T | **Y** |
| A | B | C | D | **C** | E | F | G | H | I |
| T | C | N | R | **A** | Z | W | R | M | J |
| A | T | P | X | **T** | X | T | Q | N | K |
| D | C | F | E | G | Y | S | P | O | L |

### This coding challenge is made up of two stages:

**Stage 1: 2D GRID OF ENGLISH ALPHABETS**

Write an algorithm that will stepwise:

* Create a 2D square grid of size `N`.
* Generate a list of `k` English words (with or without repetition), such that it fills up `~ x%` (Grid word density) of the cells (In the example above, the animal names `cat, cat, dog, monkey` makes up `15%` of the total no of cells).
* Place the generated words in the grid at random locations, vertically (V), horizontally (H) or diagonally (D) sampling from a placement probability distribution: `P(V)=p_1`, `P(H)=p_2`, `P(D)=p_3`.

At the end of this stage, you should have:

* A square grid of alphabets.
* A set of unique English words that are present inside the grid.

**Stage 2: SEARCH ENGLISH WORDS IN A 2D GRID**

Given a 2D alphabet grid and a set of unique words present in the grid, Write an algorithm, that will:

Search for all occurrences of the input words in the grid.

The output of this program for the example grid:

`'CAT' Found at (0, 0) in Horizontal direction`

`'DOG' Found at (3, 1) in Vertical direction`

`'CAT' Found at (6, 4) in Vertical direction`

`'MONKEY' Found at (0, 4) in Diagonal direction`

# Implementation constraints
___

1. This test is designed to be completed within 2 days. Please adhere to the **2 days deadline**. 
2. Use **python 3**.
2. Use a **git based code repository** for hosting purpose.
3. Use a **Readme** for documentation.

# Bonus points
**Bonus points** for:
* **Object oriented design**.
* Exposing the application with a **restful web API** and **dockerised distribution**.
* **Parameterisation of the application** by:
    * Size of the grid (`N`).
    * Placement probability distribution (`P`).
    * Grid word density (`x`)
* **Comments on the following**:
    * Complexity of the search algorithm with `N` and grid dimension (1D, 2D, 3D...)?
    * How did you avoid/deal with multiple words overlapping each other during word placement task (last step) in stage 1?
    * What are some of the alternate solutions of the search problem worth exploring?
    * Anything else that you suggest that will make this problem/solution more interesting?  
    
    

